<?php 

	// PHP para adicionar contenido un archivo txt con nombre, indicando por el usuario y ya existente

   
	if (!empty($_POST['txtName']) && empty($_POST['txtTexto'])) {
        $xml = new DomDocument('1.0','UTF-8');
        
        
      
		
		include '../config/configuration.php';
		$entity = "usuario";
		$con->connect();
		$query = "SELECT * FROM $entity";
		$con->setQuery($query);
		$nreg = $con->totalRecords();
		
		$registroDeLaBd = $xml->createElement('registroDeLaBd');
		$registroDeLaBd = $xml->appendChild($registroDeLaBd);
		
		while ($row = $con->getArrayRecord()) {
			$registro = $xml->createElement('registro');
			$registro = $registroDeLaBd->appendChild($registro);
			
			$id = $xml->createElement('id',$row['id']);
			$id = $registro->appendChild($id);
			$usuario = $xml->createElement('usuario',utf8_encode($row['usuario']));
			$usuario = $registro->appendChild($usuario);
			$nombre = $xml->createElement('nombre',utf8_encode($row['nombre']));
			$nombre = $registro->appendChild($nombre);
			$correo = $xml->createElement('correo',utf8_encode($row['correo']));
			$correo = $registro->appendChild($correo);
			$foto = $xml->createElement('foto',$row['foto']);
			$foto = $registro->appendChild($foto);
			$fecha_hora_registro = $xml->createElement('fecha_hora_registro',utf8_encode($row['fecha_hora_registro']));
			$fecha_hora_registro = $registro->appendChild($fecha_hora_registro);
		}
        $xml->formatOutput = true;
        $xml->saveXML();
        $xml->save('../assets/files/'.$_POST['txtName'].'.xml');
        $message = "Registro guardado.";

	}else{
		$message = "No sumintro datos suficientes para crear el archivo o introdujo texto";
	}

	echo json_encode(['message' => $message]);